import './midbar.scss';

const Midbar = () => {
  return (
    <div className='midbar'>
      <hr />
      <div className='list'>
        <div className='list1'>Default</div>
        <span>
          <ul className='breadcrumb'>
            <li>
              <a href='/#' className="icon icon-shape text-light-blue, fas fa-home">
              </a>
            </li>
            <li>
              <a href='/#'>Dashboards</a>
            </li>
            <li>Default</li>
          </ul>
          <button className='' style={{ backgroundColor: '#eee' }}>Filters</button>
          <button className='' style={{ backgroundColor: '#eee' }}>New</button>
        </span>
      </div>
    </div>
  );
};

export default Midbar;
