import { Link } from "react-router-dom";
import {
  DropdownMenu,
  UncontrolledDropdown,
  DropdownToggle,
  Nav,
  UncontrolledCollapse, Navbar,
} from "reactstrap";


import { NavLink } from "react-router-dom";

const AdminNavbar = ( {isAuthNavbar = false}) => {
if (isAuthNavbar){
  return (
    <>
      <Navbar className="navbar navbar-top navbar-horizontal navbar-dark" expand="md">
        <div className="container px-4">
          <div className="navbar-brand" to="/" tag={Link}>
            <img
              alt="..."
              src={
                require("../assets/img/brand/argon-react-white.png").default
              }
            />
          </div>
          <button className="navbar-toggler" id="navbar-collapse-main">
            <span className="navbar-toggler-icon" />
          </button>
          <UncontrolledCollapse navbar toggler="#navbar-collapse-main">
            <div className="navbar-collapse-header d-md-none">
              <div className="row">
                <div className="collapse-brand col-xs-6">
                  <Link to="/">
                    <img
                      alt="..."
                      src={
                        require("../assets/img/brand/argon-react.png")
                          .default
                      }
                    />
                  </Link>
                </div>
                <div className="collapse-close col-xs-6">
                  <button className="navbar-toggler" id="navbar-collapse-main">
                    <span />
                    <span />
                  </button>
                </div>
              </div>
            </div>
            <Nav className="ml-auto" navbar>
              <div className="nav-item">
                <NavLink className="nav-link nav-link-icon" to="/admin/dashboard" tag={Link}>
                  <i className="ni ni-planet" />
                  <span className="nav-link-inner--text">Dashboard</span>
                </NavLink>
              </div>
              <div className="nav-item">
                <NavLink
                  className="nav-link nav-link-icon"
                  to="/auth/register"
                  tag={Link}
                >
                  <i className="ni ni-circle-08" />
                  <span className="nav-link-inner--text">Register</span>
                </NavLink>
              </div>
              <div className="nav-item">
                <NavLink className="nav-link nav-link-icon" to="/auth/login" tag={Link}>
                  <i className="ni ni-key-25" />
                  <span className="nav-link-inner--text">Login</span>
                </NavLink>
              </div>
              <div className="nav-item">
                <NavLink
                  className="nav-link nav-link-icon"
                  to="/admin/user-profile"
                  tag={Link}
                >
                  <i className="ni ni-single-02" />
                  <span className="nav-link-inner--text">Profile</span>
                </NavLink>
              </div>
            </Nav>
          </UncontrolledCollapse>
        </div>
      </Navbar>
    </>
  );
}else{
  
  return (
    <>
      <div className="navbar navbar-top navbar-dark" expand="md" id="navbar-main">
        <div className="container-fluid">
          <form className="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex">
            <div className="form-group mb-0">
              <div className="input-group input-group-alternative">
                <div addonType="prepend" className="input-group-prepend">
                  <div className="input-group-text">
                    <i className="fas fa-search" />
                  </div>
                </div>
                <input placeholder="Search" type="text" className="form-control" />
              </div>
            </div>
          </form>
          <Nav className="align-items-center d-none d-md-flex" navbar>
            <UncontrolledDropdown nav>
              <DropdownToggle className="pr-0" nav>
                <div className="media align-items-center">
                  <span className="avatar avatar-sm rounded-circle">
                    <img
                      alt="..."
                      src={
                        require("../assets/img/theme/team-4.jpg")
                          .default
                      }
                    />
                  </span>
                  <div className="media ml-2 d-none d-lg-block">
                    <span className="mb-0 text-sm font-weight-bold">
                      Vimal Raj M
                    </span>
                  </div>
                </div>
              </DropdownToggle>
              <DropdownMenu className="dropdown-menu-arrow" right>
                <div className="dropdown-item noti-title" header tag="div">
                  <h6 className="text-overflow m-0">Welcome!</h6>
                </div >
                <div className="dropdown-item" to="/admin/user-profile" tag={Link}>
                  <i className="ni ni-single-02" />
                  <span>My profile</span>
                </div >
                <div className="dropdown-item" to="/admin/user-profile" tag={Link}>
                  <i className="ni ni-settings-gear-65" />
                  <span>Settings</span>
                </div >
                <div className="dropdown-item" to="/admin/user-profile" tag={Link}>
                  <i className="ni ni-calendar-grid-58" />
                  <span>Activity</span>
                </div >
                <div className="dropdown-item" to="/admin/user-profile" tag={Link}>
                  <i className="ni ni-support-16" />
                  <span>Support</span>
                </div >
                <div className="dropdown-item" />
                <div className="dropdonw-item" href="/auth/login" onClick={(e) => e.preventDefault()}>
                  <i className="ni ni-user-run" />
                  <span>Click to Logout</span>
                </div>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </div>
      </div>
    </>
  );
};

}


export default AdminNavbar;
