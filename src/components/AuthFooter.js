
const Login = ({ isNavigatedFromLogin }) => {
  return (
    <>
      <div className="container" >
        <footer className="footer" style={{ backgroundColor: isNavigatedFromLogin ? "rgba(76, 175, 80, 0)" : 'rgba(76, 175, 80, 0)' }}>
          <div className="row align-items-center justify-content-xl-between" >
            <div className="col-xl-6">
              <div className="copyright text-center text-xl-left text-muted">
                @ {new Date().getFullYear()}{" "}
                <a
                  className="font-weight-bold ml-1"
                  href="/#"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  Creative Tim
                </a>
              </div>
            </div>

            <div className="col-xl-6">
              <nav className=" navbar nav-footer justify-content-center justify-content-xl-end" >
                <div className="nav-item">
                  <div className="nav-link" 
                    href="/#"
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    Creative Tim
                  </div>
                </div>

                <div className="nav-item">
                  <div className="nav-link"
                    href="/#"
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    About Us
                  </div>
                </div>

                <div className="nav-item">
                  <div className="nav-link"
                    href="/#"
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    Blog
                  </div>
                </div>

                <div className="nav-item">
                  <div className="nav-link"
                    href="/#"
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    MIT License
                  </div>
                </div>
              </nav>
            </div>
          </div>
        </footer>
      </div>
    </>
  );
};

export default Login;
