import Midbar from "components/Midbar";
import './midbar.scss';


const Header = ({ CanShowCardBody = true , isUserHeader = false}) => {
  if (isUserHeader){
    return (
      <>
        <div
          className="header pb-8 pt-5 pt-lg-8 d-flex align-items-center"
          style={{
            minHeight: "600px",
            backgroundImage:
              "url(" +
              require("../assets/img/theme/profile-cover.jpg").default +
              ")",
            backgroundSize: "cover",
            backgroundPosition: "center top",
          }}
        >
          {/* Mask */}
          <span className="mask bg-gradient-default opacity-8" />
          {/* Header container */}
          <div className="container-fluid d-flex align-items-center">
            <div className="row">
              <div className="col-lg-7 col-md-10">
                <h1 className="display-2 text-white">Hello Vimal Raj M</h1>
                <p className="text-white mt-0 mb-5">
                  Welcome back to your profile page, you can View your Account deatils here.
                  Please try to update the Fname, Lname and Email address.
  
  
                </p>
                <button className="btn-info"
                  color="info"
                  href="#pablo"
                  onClick={(e) => e.preventDefault()}
                >
                  Edit profile
                </button>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }else{

  
  return (
    <>
      <div className="header pb-8 pt-5 pt-md-8" style={{ backgroundImage: `linear-gradient(to right, rgba(32, 32, 32), rgba(0, 24, 255, 1  ))`, }}>
        <div className="container-fluid">
          <Midbar />
          <div className="header-body">
            {/* Card stats */}
            {CanShowCardBody && <div className="row">


              <div className="col-lg-6 col-xl-3">
                <div className="card card-stats mb-4 mb-xl-0">
                  <div className="card-body">
                    <div className="row">
                      <div className="col">
                        <div
                          tag="h5"
                          className="card-titletext-uppercase text-muted mb-0"
                        >
                          Total Records
                        </div>
                        <span className="h2 font-weight-bold mb-0">
                          50,897
                        </span>
                      </div>
                      <div className="col-auto">
                        <div className="icon icon-shape bg-danger text-white rounded-circle shadow">
                          <i className="fas fa-chart-bar" />
                        </div>
                      </div>

                      <p className="mt-3 mb-0 text-muted text-sm">
                        <span className="text-success mr-2">
                          <i className="fa fa-arrow-up" /> 1.48%
                        </span>{" "}
                        <span className="text-nowrap">Since last month</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              {/* //2nd// */}
              <div className="col-lg-6 col-xl-3">
                <div className="card card-stats mb-4 mb-xl-0">
                  <div className="card-body">
                    <div className="row">
                      <div className="col">
                        <div
                          tag="h5"
                          className="card-titletext-uppercase text-muted mb-0"
                        >
                          New users
                        </div>
                        <span className="h2 font-weight-bold mb-0">
                          1,566
                        </span>
                      </div>
                      <div className="col-auto">
                        <div className="icon icon-shape bg-danger text-white rounded-circle shadow">
                          <i className="fas fa-chart-pie" />
                        </div>
                      </div>

                      <p className="mt-3 mb-0 text-muted text-sm">
                        <span className="text-danger mr-2">
                          <i className="fas fa-arrow-down" /> 5.48%
                        </span>{" "}
                        <span className="text-nowrap">Since last week</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              {/* 3rd */}
              <div className="col-lg-6 col-xl-3">
                <div className="card card-stats mb-4 mb-xl-0">
                  <div className="card-body">
                    <div className="row">
                      <div className="col">
                        <div
                          tag="h5"
                          className="card-title text-muted mb-0"
                        >
                          Sales
                        </div>
                        <span className="h2 font-weight-bold mb-0">
                          1000
                        </span>
                      </div>
                      <div className="col-auto">
                        <div className="icon icon-shape bg-danger text-white rounded-circle shadow">
                          <i className="fas fa-users" />
                        </div>
                      </div>

                      <p className="mt-3 mb-0 text-muted text-sm">
                        <span className="text-warning mr-2">
                          <i className="fas fa-arrow-down" /> 1.0%
                        </span>{" "}
                        <span className="text-nowrap">Since yesterday</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              {/* 4th  */}
              <div className="col-lg-6 col-xl-3">
                <div className="card card-stats mb-4 mb-xl-0">
                  <div className="card-body">
                    <div className="row">
                      <div className="col">
                        <div
                          tag="h5"
                          className="card-title text-muted mb-0"
                        >
                          Performance
                        </div>
                        <span className="h2 font-weight-bold mb-0">
                          56%
                        </span>
                      </div>
                      <div className="col-auto">
                        <div className="icon icon-shape bg-danger text-white rounded-circle shadow">
                          <i className="fas fa-percent" />
                        </div>
                      </div>

                      <p className="mt-3 mb-0 text-muted text-sm">
                        <span className="text-success mr-2">
                          <i className="fa fa-arrow-up" /> 2.70%
                        </span>{" "}
                        <span className="text-nowrap">Since last month</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>}
          </div>
        </div>
      </div>
    </>
  );
};
}

export default Header;
