import Admin from "layouts/Admin";
import Auth from "layouts/Auth";
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import "assets/plugins/nucleo/css/nucleo.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "assets/scss/argon-dashboard-react.scss";
import "./assets/css/custom.css"


ReactDOM.render(
  <BrowserRouter>
    <Switch>
      {/* <AdminLayout/> */}
      <Route path="/admin" render={(props) => <Admin {...props} />} />
      <Route path="/auth" render={(props) => <Auth {...props} />} />
      <Redirect from="/" to="/auth/login" />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")

);
