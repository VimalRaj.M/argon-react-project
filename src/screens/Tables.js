import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  Progress,
  Table,
  UncontrolledTooltip,
} from "reactstrap";

import Header from "components/Header.js";


const CardTableView = () => {
  return (<Table className="align-items-center table-flush" responsive>
    <thead className="thead-light">
      <tr>
        <th scope="col">Project</th>
        <th scope="col">Budget</th>
        <th scope="col">Status</th>
        <th scope="col">Users</th>
        <th scope="col">Completion</th>
        <th scope="col" />
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">
          <Media className="align-items-center">
            <a
              className="avatar rounded-circle mr-3"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                src={
                  require("../assets/img/theme/bootstrap.jpg")
                    .default
                }
              />
            </a>
            <Media>
              <span className="mb-0 text-sm">
                Argon Design System
              </span>
            </Media>
          </Media>
        </th>
        <td>$600 USD</td>
        <td>
          <div color="" className="badge badge-dot mr-4">
            <i className="bg-warning" />
            pending
          </div>
        </td>
        <td>
          <div className="avatar-group">
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip742438047"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-1-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip742438047"
            >
              Tattoo Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip941738690"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-2-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip941738690"
            >
              Cartoon Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip804044742"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-3-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip804044742"
            >
              Samantha
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip996637554"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-4.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip996637554"
            >
              Vimal Raj M
            </UncontrolledTooltip>
          </div>
        </td>
        <td>
          <div className="d-flex align-items-center">
            <span className="mr-2">60%</span>
            <div>
              <Progress
                max="100"
                value="60"
                barClassName="bg-danger"
              />
            </div>
          </div>
        </td>
        <td className="text-right">
          <UncontrolledDropdown>
            <DropdownToggle
              className="btn-icon-only text-light"
              href="#pablo"
              role="button"
              size="sm"
              color=""
              onClick={(e) => e.preventDefault()}
            >
              <i className="fas fa-ellipsis-v" />
            </DropdownToggle>
            <DropdownMenu className="dropdown-menu-arrow" right>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Another action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Something else here
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </td>
      </tr>
      <tr>
        <th scope="row">
          <Media className="align-items-center">
            <a
              className="avatar rounded-circle mr-3"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                src={
                  require("../assets/img/theme/angular.jpg")
                    .default
                }
              />
            </a>
            <Media>
              <span className="mb-0 text-sm">
                Angular Now UI Kit PRO
              </span>
            </Media>
          </Media>
        </th>
        <td>$1000 USD</td>
        <td>
          <div color="" className="badge badge-dot">
            <i className="bg-success" />
            completed
          </div>
        </td>
        <td>
          <div className="avatar-group">
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip746418347"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-1-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip746418347"
            >
              Tattoo Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip102182364"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-2-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip102182364"
            >
              Cartoon Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip406489510"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-3-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip406489510"
            >
              Samantha
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip476840018"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-4.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip476840018"
            >
              Vimal Raj M
            </UncontrolledTooltip>
          </div>
        </td>
        <td>
          <div className="d-flex align-items-center">
            <span className="mr-2">100%</span>
            <div>
              <Progress
                max="100"
                value="100"
                barClassName="bg-success"
              />
            </div>
          </div>
        </td>
        <td className="text-right">
          <UncontrolledDropdown>
            <DropdownToggle
              className="btn-icon-only text-light"
              href="#pablo"
              role="button"
              size="sm"
              color=""
              onClick={(e) => e.preventDefault()}
            >
              <i className="fas fa-ellipsis-v" />
            </DropdownToggle>
            <DropdownMenu className="dropdown-menu-arrow" right>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Another action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Something else here
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </td>
      </tr>
      <tr>
        <th scope="row">
          <Media className="align-items-center">
            <a
              className="avatar rounded-circle mr-3"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                src={
                  require("../assets/img/theme/sketch.jpg")
                    .default
                }
              />
            </a>
            <Media>
              <span className="mb-0 text-sm">Black Dashboard</span>
            </Media>
          </Media>
        </th>
        <td>$720 USD</td>
        <td>
          <div color="" className="badge badge-dot mr-4">
            <i className="bg-danger" />
            delayed
          </div>
        </td>
        <td>
          <div className="avatar-group">
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip753056318"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-1-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip753056318"
            >
              Tattoo Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip441753266"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-2-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip441753266"
            >
              Cartoon Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip188462246"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-3-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip188462246"
            >
              Samantha
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip621168444"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-4.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip621168444"
            >
              Vimal Raj M
            </UncontrolledTooltip>
          </div>
        </td>
        <td>
          <div className="d-flex align-items-center">
            <span className="mr-2">72%</span>
            <div>
              <Progress
                max="100"
                value="72"
                barClassName="bg-danger"
              />
            </div>
          </div>
        </td>
        <td className="text-right">
          <UncontrolledDropdown>
            <DropdownToggle
              className="btn-icon-only text-light"
              href="#pablo"
              role="button"
              size="sm"
              color=""
              onClick={(e) => e.preventDefault()}
            >
              <i className="fas fa-ellipsis-v" />
            </DropdownToggle>
            <DropdownMenu className="dropdown-menu-arrow" right>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Another action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Something else here
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </td>
      </tr>
      <tr>
        <th scope="row">
          <Media className="align-items-center">
            <a
              className="avatar rounded-circle mr-3"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                src={
                  require("../assets/img/theme/react.jpg")
                    .default
                }
              />
            </a>
            <Media>
              <span className="mb-0 text-sm">
                React Material Dashboard
              </span>
            </Media>
          </Media>
        </th>
        <td>$900 USD</td>
        <td>
          <div color="" className="badge badge-dot">
            <i className="bg-info" />
            on schedule
          </div>
        </td>
        <td>
          <div className="avatar-group">
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip875258217"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-1-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip875258217"
            >
              Tattoo Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip834416663"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-2-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip834416663"
            >
              Cartoon Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip372449339"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-3-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip372449339"
            >
              Samantha
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip108714769"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-4.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip108714769"
            >
              Vimal Raj M
            </UncontrolledTooltip>
          </div>
        </td>
        <td>
          <div className="d-flex align-items-center">
            <span className="mr-2">90%</span>
            <div>
              <Progress
                max="100"
                value="90"
                barClassName="bg-info"
              />
            </div>
          </div>
        </td>
        <td className="text-right">
          <UncontrolledDropdown>
            <DropdownToggle
              className="btn-icon-only text-light"
              href="#pablo"
              role="button"
              size="sm"
              color=""
              onClick={(e) => e.preventDefault()}
            >
              <i className="fas fa-ellipsis-v" />
            </DropdownToggle>
            <DropdownMenu className="dropdown-menu-arrow" right>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Another action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Something else here
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </td>
      </tr>
      <tr>
        <th scope="row">
          <Media className="align-items-center">
            <a
              className="avatar rounded-circle mr-3"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                src={
                  require("../assets/img/theme/vue.jpg").default
                }
              />
            </a>
            <Media>
              <span className="mb-0 text-sm">
                Vue Paper UI Kit PRO
              </span>
            </Media>
          </Media>
        </th>
        <td>$2,200 USD</td>
        <td>
          <div color="" className="badge badge-dot mr-4">
            <i className="bg-success" />
            completed
          </div>
        </td>
        <td>
          <div className="avatar-group">
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip664029969"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-1-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip664029969"
            >
              Tattoo Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip806693074"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-2-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip806693074"
            >
              Cartoon Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip844096937"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-3-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip844096937"
            >
              Samantha
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip757459971"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-4.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip757459971"
            >
              Vimal Raj M
            </UncontrolledTooltip>
          </div>
        </td>
        <td>
          <div className="d-flex align-items-center">
            <span className="mr-2">100%</span>
            <div>
              <Progress
                max="100"
                value="100"
                barClassName="bg-success"
              />
            </div>
          </div>
        </td>
        <td className="text-right">
          <UncontrolledDropdown>
            <DropdownToggle
              className="btn-icon-only text-light"
              href="#pablo"
              role="button"
              size="sm"
              color=""
              onClick={(e) => e.preventDefault()}
            >
              <i className="fas fa-ellipsis-v" />
            </DropdownToggle>
            <DropdownMenu className="dropdown-menu-arrow" right>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Another action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Something else here
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </td>
      </tr>
    </tbody>
  </Table>);
}

const CardTableFooterView = () => {
  return (<div className="card-footer py-4">
    <nav aria-label="...">
      <div
        className="pagination justify-content-end mb-0"
        listClassName="justify-content-end mb-0"
      >
        <div className="page-item disabled">
          <div className="page-link"
            href="#pablo"
            onClick={(e) => e.preventDefault()}
            tabIndex="-1"
          >
            <i className="fas fa-angle-left" />
            <span className="sr-only">Previous</span>
          </div>
        </div>
        <div className="page-item active">
          <div className="page-link"
            href="#pablo"
            onClick={(e) => e.preventDefault()}
          >
            1
          </div>
        </div>
        <div className="page-item">
          <div className="page-link"
            href="#pablo"
            onClick={(e) => e.preventDefault()}
          >
            2 <span className="sr-only">(current)</span>
          </div>
        </div>
        <div className="page-item">
          <div className="page-link"
            href="#pablo"
            onClick={(e) => e.preventDefault()}
          >
            3
          </div>
        </div>
        <div className="page-item">
          <div className="page-link"
            href="#pablo"
            onClick={(e) => e.preventDefault()}
          >
            <i className="fas fa-angle-right" />
            <span className="sr-only">Next</span>
          </div>
        </div>
      </div>
    </nav>
  </div>);
}

const DarkTableView = () => {
  return (<Table
    className="align-items-center table-dark table-flush"
    responsive
  >
    <thead className="thead-dark">
      <tr>
        <th scope="col">Project</th>
        <th scope="col">Budget</th>
        <th scope="col">Status</th>
        <th scope="col">Users</th>
        <th scope="col">Completion</th>
        <th scope="col" />
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">
          <Media className="align-items-center">
            <a
              className="avatar rounded-circle mr-3"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                src={
                  require("../assets/img/theme/bootstrap.jpg")
                    .default
                }
              />
            </a>
            <Media>
              <span className="mb-0 text-sm">
                Argon Design System
              </span>
            </Media>
          </Media>
        </th>
        <td>$600 USD</td>
        <td>
          <div color="" className="badge badge-dot mr-4">
            <i className="bg-warning" />
            pending
          </div>
        </td>
        <td>
          <div className="avatar-group">
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip731399078"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-1-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip731399078"
            >
              Tattoo Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip491083084"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-2-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip491083084"
            >
              Cartoon Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip528540780"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-3-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip528540780"
            >
              Samantha
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip237898869"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-4.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip237898869"
            >
              Vimal Raj M
            </UncontrolledTooltip>
          </div>
        </td>
        <td>
          <div className="d-flex align-items-center">
            <span className="mr-2">60%</span>
            <div>
              <Progress
                max="100"
                value="60"
                barClassName="bg-warning"
              />
            </div>
          </div>
        </td>
        <td className="text-right">
          <UncontrolledDropdown>
            <DropdownToggle
              className="btn-icon-only text-light"
              href="#pablo"
              role="button"
              size="sm"
              color=""
              onClick={(e) => e.preventDefault()}
            >
              <i className="fas fa-ellipsis-v" />
            </DropdownToggle>
            <DropdownMenu className="dropdown-menu-arrow" right>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Another action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Something else here
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </td>
      </tr>
      <tr>
        <th scope="row">
          <Media className="align-items-center">
            <a
              className="avatar rounded-circle mr-3"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                src={
                  require("../assets/img/theme/angular.jpg")
                    .default
                }
              />
            </a>
            <Media>
              <span className="mb-0 text-sm">
                Angular Now UI Kit PRO
              </span>
            </Media>
          </Media>
        </th>
        <td>$1000 USD</td>
        <td>
          <div color="" className="badge badge-dot">
            <i className="bg-success" />
            completed
          </div>
        </td>
        <td>
          <div className="avatar-group">
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip188614932"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-1-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip188614932"
            >
              Tattoo Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip66535734"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-2-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip delay={0} target="tooltip66535734">
              Cartoon Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip427561578"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-3-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip427561578"
            >
              Samantha
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip904098289"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-4.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip904098289"
            >
              Vimal Raj M
            </UncontrolledTooltip>
          </div>
        </td>
        <td>
          <div className="d-flex align-items-center">
            <span className="mr-2">100%</span>
            <div>
              <Progress
                max="100"
                value="100"
                barClassName="bg-success"
              />
            </div>
          </div>
        </td>
        <td className="text-right">
          <UncontrolledDropdown>
            <DropdownToggle
              className="btn-icon-only text-light"
              href="#pablo"
              role="button"
              size="sm"
              color=""
              onClick={(e) => e.preventDefault()}
            >
              <i className="fas fa-ellipsis-v" />
            </DropdownToggle>
            <DropdownMenu className="dropdown-menu-arrow" right>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Another action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Something else here
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </td>
      </tr>
      <tr>
        <th scope="row">
          <Media className="align-items-center">
            <a
              className="avatar rounded-circle mr-3"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                src={
                  require("../assets/img/theme/sketch.jpg")
                    .default
                }
              />
            </a>
            <Media>
              <span className="mb-0 text-sm">Black Dashboard</span>
            </Media>
          </Media>
        </th>
        <td>$712 USD</td>
        <td>
          <div color="" className="badge badge-dot mr-4">
            <i className="bg-danger" />
            delayed
          </div>
        </td>
        <td>
          <div className="avatar-group">
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip707904950"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-1-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip707904950"
            >
              Tattoo Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip353988222"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-2-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip353988222"
            >
              Cartoon Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip467171202"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-3-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip467171202"
            >
              Samantha
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip362118155"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-4.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip362118155"
            >
              Vimal Raj M
            </UncontrolledTooltip>
          </div>
        </td>
        <td>
          <div className="d-flex align-items-center">
            <span className="mr-2">72%</span>
            <div>
              <Progress
                max="100"
                value="72"
                barClassName="bg-danger"
              />
            </div>
          </div>
        </td>
        <td className="text-right">
          <UncontrolledDropdown>
            <DropdownToggle
              className="btn-icon-only text-light"
              href="#pablo"
              role="button"
              size="sm"
              color=""
              onClick={(e) => e.preventDefault()}
            >
              <i className="fas fa-ellipsis-v" />
            </DropdownToggle>
            <DropdownMenu className="dropdown-menu-arrow" right>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Another action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Something else here
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </td>
      </tr>
      <tr>
        <th scope="row">
          <Media className="align-items-center">
            <a
              className="avatar rounded-circle mr-3"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                src={
                  require("../assets/img/theme/react.jpg")
                    .default
                }
              />
            </a>
            <Media>
              <span className="mb-0 text-sm">
                React Material Dashboard
              </span>
            </Media>
          </Media>
        </th>
        <td>$900 USD</td>
        <td>
          <div color="" className="badge badge-dot">
            <i className="bg-info" />
            on schedule
          </div>
        </td>
        <td>
          <div className="avatar-group">
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip226319315"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-1-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip226319315"
            >
              Tattoo Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip711961370"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-2-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip711961370"
            >
              Cartoon Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip216246707"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-3-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip216246707"
            >
              Samantha
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip638048561"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-4.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip638048561"
            >
              Vimal Raj M
            </UncontrolledTooltip>
          </div>
        </td>
        <td>
          <div className="d-flex align-items-center">
            <span className="mr-2">90%</span>
            <div>
              <Progress
                max="100"
                value="90"
                barClassName="bg-info"
              />
            </div>
          </div>
        </td>
        <td className="text-right">
          <UncontrolledDropdown>
            <DropdownToggle
              className="btn-icon-only text-light"
              href="#pablo"
              role="button"
              size="sm"
              color=""
              onClick={(e) => e.preventDefault()}
            >
              <i className="fas fa-ellipsis-v" />
            </DropdownToggle>
            <DropdownMenu className="dropdown-menu-arrow" right>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Another action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Something else here
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </td>
      </tr>
      <tr>
        <th scope="row">
          <Media className="align-items-center">
            <a
              className="avatar rounded-circle mr-3"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                src={
                  require("../assets/img/theme/vue.jpg").default
                }
              />
            </a>
            <Media>
              <span className="mb-0 text-sm">
                Vue Paper UI Kit PRO
              </span>
            </Media>
          </Media>
        </th>
        <td>$1000 USD</td>
        <td>
          <div color="" className="badge badge-dot mr-4">
            <i className="bg-success" />
            completed
          </div>
        </td>
        <td>
          <div className="avatar-group">
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip781594051"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-1-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip781594051"
            >
              Tattoo Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip840372212"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-2-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip840372212"
            >
              Cartoon Man
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip497647175"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-3-800x800.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip497647175"
            >
              Samantha
            </UncontrolledTooltip>
            <a
              className="avatar avatar-sm"
              href="#pablo"
              id="tooltip951447946"
              onClick={(e) => e.preventDefault()}
            >
              <img
                alt="..."
                className="rounded-circle"
                src={
                  require("../assets/img/theme/team-4.jpg")
                    .default
                }
              />
            </a>
            <UncontrolledTooltip
              delay={0}
              target="tooltip951447946"
            >
              Vimal Raj M
            </UncontrolledTooltip>
          </div>
        </td>
        <td>
          <div className="d-flex align-items-center">
            <span className="mr-2">100%</span>
            <div>
              <Progress
                max="100"
                value="100"
                barClassName="bg-danger"
              />
            </div>
          </div>
        </td>
        <td className="text-right">
          <UncontrolledDropdown>
            <DropdownToggle
              className="btn-icon-only text-light"
              href="#pablo"
              role="button"
              size="sm"
              color=""
              onClick={(e) => e.preventDefault()}
            >
              <i className="fas fa-ellipsis-v" />
            </DropdownToggle>
            <DropdownMenu className="dropdown-menu-arrow" right>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Another action
              </DropdownItem>
              <DropdownItem
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                Something else here
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </td>
      </tr>
    </tbody>
  </Table>);
}


const Tables = () => {
  return (
    <>
      <Header CanShowCardBody={false} />
      {/* Page content */}
      <div className="container-fluid mt--7">
        {/* Table */}
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-header border-0">
                <h3 className="mb-0">Card tables</h3>
              </div>
              <CardTableView />
              <CardTableFooterView />
            </div>
          </div>
        </div>
        {/* Dark table */}
        <div className="row mt-5">
          <div className="col">
            <div className="card bg-default shadow">
              <div className="card-header bg-transparent border-0">
                <h3 className="text-white mb-0">Dark tables</h3>
              </div>
              <DarkTableView />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Tables;
