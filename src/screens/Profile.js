import Header from "components/Header";





const MyProfileView = () => {
  return (
    <div className="order-xl-2 mb-5 mb-xl-0 col-xl-4">
      <div className="card card-profile shadow">
        <div className="row justify-content-center">
          <div className="order-lg-2 col-lg-3">
            <div className="card-profile-image">
              <a href="#pablo" onClick={(e) => e.preventDefault()}>
                <img
                  alt="..."
                  className="rounded-circle"
                  src={
                    require("../assets/img/theme/team-4.jpg")
                      .default
                  }
                />
              </a>
            </div>
          </div>
        </div>
        <div className="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4" >
          <div className="d-flex justify-content-between" >
            <button
              className="btn-primary mr-4" style={{ zIndex: 1 }}
              color="info"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
              size="sm"
            >
              Connect
            </button>
            <button
              className="btn-success float-right" style={{ zIndex: 1 }}
              color="default"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
              size="sm"
            >
              Message
            </button>
          </div>
        </div>
        <div className="card-body pt-0 pt-md-4">
          <div className="row">
            <div className="col">
              <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                <div>
                  <span className="heading">22</span>
                  <span className="description">Friends</span>
                </div>
                <div>
                  <span className="heading">10</span>
                  <span className="description">Photos</span>
                </div>
                <div>
                  <span className="heading">89</span>
                  <span className="description">Comments</span>
                </div>
              </div>
            </div>
          </div>
          <div className="text-center">
            <h3>
              Vimal Raj M
              <span className="font-weight-light">, 24</span>
            </h3>
            <div className="h5 font-weight-300">
              <i className="ni location_pin mr-2" />
              India, Bangalore
            </div>
            <div className="h5 mt-4">
              <i className="ni business_briefcase-24 mr-2" />
              Support Engineer - Software Engineer
            </div>
            <div>
              <i className="ni education_hat mr-2" />
              PVP(Computer Science)
            </div>
            <hr className="my-4" />
            <p>
              Vimal — Works hard and try to achieve his Goal
            </p>
            <a href="#pablo" onClick={(e) => e.preventDefault()}>
              Show more
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}


const MyaccountInfoView = () => {
  return (
    <div className="pl-lg-4">
      <div className="row">
        <div
          className="col-lg-6">
          <div>
            <label
              className="form-control-label"
              htmlFor="input-username"
            >
              Username
            </label>
            <input
              className="form-control form-control-alternative"
              defaultValue="Vimal.Raj"
              id="input-username"
              placeholder="Username"
              type="text"
            />
          </div>
        </div>
        <div className="col-lg-6">
          <div className="form-group">
            <label
              className="form-control-label"
              htmlFor="input-email"
            >
              Email address
            </label>
            <input
              className="form-control form-control-alternative"
              id="input-email"
              placeholder="vimal@example.com"
              type="email"
            />
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-6">
          <div className="form-group">
            <label
              className="form-control-label"
              htmlFor="input-first-name"
            >
              First name
            </label>
            <input
              className="form-control form-control-alternative"
              defaultValue="Vimal"
              id="input-first-name"
              placeholder="First name"
              type="text"
            />
          </div>
        </div>
        <div className="col-lg-6">
          <div className="from-group">
            <label
              className="form-control-label"
              htmlFor="input-last-name"
            >
              Last name
            </label>
            <input
              className="form-control form-control-alternative"
              defaultValue="Raj M"
              id="input-last-name"
              placeholder="Last name"
              type="text"
            />
          </div>
        </div>
      </div>
    </div>
  );
}

const MyContactInfoView = () => {
  return (<div className="pl-lg-4">
    <div className="row">
      <div className="col-md-12">
        <div className="from-group">
          <label
            className="form-control-label"
            htmlFor="input-address"
          >
            Address
          </label>
          <input
            className="form-control form-control-alternative"
            defaultValue=" Bangalore India"
            id="input-address"
            placeholder="Home Address"
            type="text"
          />
        </div>
      </div>
    </div>

    <div className="row" >
      <div className="col-lg-4">
        <div style={{ paddingTop: 20 }} className="from-group">
          <label
            className="form-control-label"
            htmlFor="input-city"
          >
            City
          </label>
          <input
            className="form-control form-control-alternative"
            defaultValue="Bangalore"
            id="input-city"
            placeholder="City"
            type="text"
          />
        </div>
      </div>
      <div className="col-lg-4">
        <div style={{ paddingTop: 20 }} className="from-group">
          <label
            className="form-control-label"
            htmlFor="input-country"
          >
            Country
          </label>
          <input
            className=" form-control form-control-alternative"
            defaultValue="India"
            id="input-country"
            placeholder="Country"
            type="text"
          />
        </div>
      </div>
      <div className="col-lg-4">
        <div style={{ paddingTop: 20 }} >
          <label
            className="form-control-label"
            htmlFor="input-country"
          >
            Postal code
          </label>
          <input
            className="form-control form-control-alternative"
            id="input-postal-code"
            placeholder="Postal code"
            type="number"
          />
        </div>
      </div>
    </div>
  </div>);
}

const MyAboutMeInfoView = () => {
  return (<div className="pl-lg-4">
    <div className="form-group">
      <label>About Me</label>
      <textarea
        className="form-control form-control-alternative" cols="70"
        placeholder="A few words about you ..."
        rows="4"
        defaultValue=" "
        type="textarea"
      />
    </div>
  </div>);
}

const Profile = () => {
  return (
    <>
      <Header isUserHeader = {true}/>
      {/* Page content */}
      <div className="container-fluid mt--7">
        <div className="row">
          <MyProfileView />
          <div className="order-xl-1 col-xl-8">
            <div className="card bg-secondary shadow">
              <div className="card-header bg-white border-0">
                <div className="row align-items-center ">
                  <div className="col-lg-8 col-md-8 col-xs-8">
                    <h3 className="mb-0">My account</h3>
                  </div>
                  <div className="text-right col-lg-4 col-md-4 col-xs-4" >
                    <button className="btn-primary"
                      color="primary"
                      href="#pablo"
                      onClick={(e) => e.preventDefault()}
                      size="sm"
                    >
                      Settings
                    </button>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <form>
                  <h6 className="heading-small text-muted mb-4">
                    User information
                  </h6>
                  <MyaccountInfoView />
                  <hr className="my-4" />
                  {/* Address */}
                  <h6 className="heading-small text-muted mb-4">
                    Contact information
                  </h6>
                  <MyContactInfoView />
                  <hr className="my-4" />
                  {/* Description */}
                  <h6 className="heading-small text-muted mb-4">About me</h6>
                  <MyAboutMeInfoView />

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Profile;
