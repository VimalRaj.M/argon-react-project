const RegisterButtonBlock = () => {
  return (<div className="text-center" style={{ display: "flex", justifyContent: "space-around" }}>
    <div className="row-md-6">
      <button
        className="btn-neutral btn-icon "
        color="default"
        href="/#pablo"
        onClick={(e) => e.preventDefault()}
      >
        <span className="">
          <img
            alt="..."
            src={
              require("../assets/img/icons/common/github.svg")
                .default
            }
          />
        </span>
        <span className="">Github</span>
      </button></div>
    <div className="row-md-6 text-right">
      <button
        className="btn-neutral btn-icon"
        color="default"
        href="/#pablo"
        onClick={(e) => e.preventDefault()}
      >
        <span className="">
          <img
            alt="..."
            src={
              require("../assets/img/icons/common/google.svg")
                .default
            }
          />
        </span>
        <span className="">Google</span>
      </button></div>
  </div>);
}

const RegisterFormBlock = () => {
  return (<div className="card-body px-lg-5 py-lg-5">
    <div className="text-center text-muted mb-4">
      <small>Or sign up with credentials</small>
    </div>
    <form className="form">
      <div className="form-group">
        <div className="input-group input-group-alternative">
          <div addonType="prepend" className="input-group-prepend">
            <div className="input-group-text">
              <i className="ni ni-hat-3" />
            </div>
          </div>
          <input placeholder="Name" type="text" className="form-control" />
        </div>
      </div>
      <div className="form-group">
        <div className="input-group input-group-alternative">
          <div addonType="prepend" className="input-group-prepend">
            <div className="input-group-text">
              <i className="ni ni-email-83" />
            </div>
          </div>
          <input
            placeholder="Email"
            type="email"
            className="form-control"
            autoComplete="new-email"
          />
        </div>
      </div>
      <div className="form-group">
        <div className="input-group input-group-alternative">
          <div addonType="prepend" className="input-group-prepend">
            <div className="input-group-text">
              <i className="ni ni-lock-circle-open" />
            </div>
          </div>
          <input
            placeholder="Password"
            type="password"
            className="form-control"
            autoComplete="new-password"
          />
        </div>
      </div>
      <div className="text-muted font-italic">
        <small>
          password strength:{" "}
          <span className="text-success font-weight-700">strong</span>
        </small>
      </div>
      <div className="my-4">
        <div xs="12">
          <div className="custom-control custom-control-alternative custom-checkbox">
            <input
              className="custom-control-input"
              id="customCheckRegister"
              type="checkbox"
            />
            <label
              className="custom-control-label"
              htmlFor="customCheckRegister"
            >
              <span className="text-muted">
                I agree with the{"  "}
                <a href="/#pablo" onClick={(e) => e.preventDefault()}>
                  Privacy Policy
                </a>
              </span>
            </label>
          </div>
        </div>
      </div>
      <div className="text-center">
        <button className="btn-primary mt-4" color="primary" type="button">
          Create account
        </button>
      </div>
    </form>
  </div>);
}
const Register = () => {
  return (
    <>
      <div className="col-lg-5 col-md-7">
        <div className="card bg-secondary shadow border-0">
          <div className="card-header bg-transparent pb-5">
            <div className="text-muted text-center mt-2 mb-4">
              <small>Sign up with</small>
            </div>
            <RegisterButtonBlock />
          </div>
          <RegisterFormBlock />
        </div>
      </div>
    </>
  );
};

export default Register;
