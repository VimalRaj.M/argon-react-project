import { useState } from "react";
import Header from "components/Header.js";

import GenericIconBlock from './generic-icon';







const Icons = () => {
  const [copiedText, setCopiedText] = useState();
  return (
    <>
      <Header CanShowCardBody={false} />

      <div className="container-fluid mt--7">

        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-header bg-transparent">
                <h3 className="mb-0">Icons</h3>
              </div>
              <div className="card-body">
                <div className="row icon-examples">
                  {/* <GenericIconBlock copiedText={copiedText} setCopiedText={setCopiedText} id={'tooltip982655500'} iconStyle={"gg-facebook"} label={'Facebook'} /> */}
                  <GenericIconBlock copiedText={copiedText} onCopyText={(text) => {
                    console.log('on return====>');
                    console.log(text)
                    setCopiedText(text);
                  }} id={'tooltip603604642'} iconStyle={"ni ni-spaceship"} label={'spaceship'} />

                  <GenericIconBlock id={'tooltip932383030'} iconStyle={"ni ni-world-2"} label={'world-2'} />
                  <GenericIconBlock id={'tooltip330279137'} iconStyle={"ni ni-tv-2"} label={'tv-2'} />
                  <GenericIconBlock id={'tooltip412313570'} iconStyle={"ni ni-umbrella-13"} label={'umbrella-13'} />
                  <GenericIconBlock id={'tooltip153036405'} iconStyle={"ni ni-square-pin"} label={'square-pin'} />
                  <GenericIconBlock id={'tooltip906422211'} iconStyle={"ni ni-support-16"} label={'support-16'} />
                  <GenericIconBlock id={'tooltip517579618'} iconStyle={"ni ni-tablet-button"} label={'tablet-button'} />
                  <GenericIconBlock id={'tooltip297195808'} iconStyle={"ni ni-tag"} label={'tag'} />
                  <GenericIconBlock id={'tooltip793084796'} iconStyle={"ni ni-tie-bow"} label={'tie-bow'} />
                  <GenericIconBlock id={'tooltip258891035'} iconStyle={"ni ni-time-alarm"} label={'time-alarm'} />
                  <GenericIconBlock id={'tooltip881235890'} iconStyle={"ni ni-trophy"} label={'trophy'} />
                  <GenericIconBlock id={'tooltip71164138'} iconStyle={"ni ni-vector"} label={'vector'} />
                  <GenericIconBlock id={'tooltip495578192'} iconStyle={"ni ni-watch-time"} label={'watch-time'} />
                  <GenericIconBlock id={'tooltip604848245'} iconStyle={"ni ni-world"} label={'world'} />
                  <GenericIconBlock id={'tooltip47550434'} iconStyle={"ni ni-air-baloon"} label={'air-baloon'} />
                  <GenericIconBlock id={'tooltip916423293'} iconStyle={"ni ni-zoom-split-in"} label={'zoom-split-in'} />

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Icons;
