
const ButtonsBlock = () => {
  return (
    <div className="text-center" style={{ display: "flex", justifyContent: "space-around" }}>
      <div className="row-md-6">
        <button
          className="btn-neutral btn-icon"
          color="default"
          href="/#pablo"
          onClick={(e) => e.preventDefault()}
        >
          <span className="">
            <img
              alt="..."
              src={
                require("../assets/img/icons/common/github.svg")
                  .default
              }
            />
          </span>
          <span className="">Github</span>
        </button></div>
      <div className="row-md-6 text-right">
        <button
          className="btn-neutral btn-icon"
          color="default"
          href="/#pablo"
          onClick={(e) => e.preventDefault()}
        >
          <span className="">
            <img
              alt="..."
              src={
                require("../assets/img/icons/common/google.svg")
                  .default
              }
            />
          </span>
          <span className="">Google</span>
        </button></div>
    </div>
  );
}

const FormBlock = () => {
  return (
    <div className="card-body px-lg-5 py-lg-5">
      <div className="text-center text-muted mb-4">
        <small>Or sign in with credentials</small>
      </div>
      <form className="form">
        <div className="form-group mb-3">
          <div className=" input-group input-group-alternative">
            <div addonType="prepend" className="input-group-prepend">
              <div className="input-group-text">
                <i className="ni ni-email-83" />
              </div>
            </div>
            <input
              placeholder="Email"
              type="email"
              className="form-control"
              autoComplete="new-email"
            />
          </div>
        </div>
        <div className="form-group mb-3">
          <div className=" input-group input-group-alternative">
            <div addonType="prepend" className="input-group-prepend">
              <div className="input-group-text">
                <i className="ni ni-lock-circle-open" />
              </div>

            </div>
            <input
              placeholder="Password"
              type="password"
              className="form-control"
              autoComplete="new-password"
            />
          </div>
        </div>
        <div className="custom-control custom-control-alternative custom-checkbox">
          <input
            className="custom-control-input"
            id=" customCheckLogin"
            type="checkbox"
          />
          <label
            className="custom-control-label"
            htmlFor=" customCheckLogin"
          >
            <span className="text-muted">Remember me</span>
          </label>
        </div>
        <div className="text-center">
          <button className="my-4 btn-primary" color="primary" type="button">
            Sign in
          </button>
        </div>
      </form>
    </div>
  );
}

const CreateUserBlock = () => {
  return (
    <div className="mt-3 row flex-nowrap" >
      <div className="col-md-6 col-xs-6 col-sm-6">
        <a
          className="text-light"
          href="/#pablo"
          onClick={(e) => e.preventDefault()}
        >
          <small>Forgot password?</small>
        </a>
      </div>
      <div className="text-right  col-md-6 col-xs-6 col-sm-6">
        <a
          className="text-light"
          href="/#pablo"
          onClick={(e) => e.preventDefault()}
        >
          <small>Create new account</small>
        </a>
      </div>
    </div>);
}

const Login = () => {

  return (
    <>
      <div className="col-lg-5 col-md-7" >
        <div className="card bg-secondary shadow border-0" >
          <div className="card-header bg-transparent pb-5">
            <div className="text-muted text-center mt-2 mb-3">
              <small>Sign in with</small>
            </div>
            <ButtonsBlock />
          </div>
          <FormBlock />
        </div>
        <CreateUserBlock />
      </div>
    </>
  );
};

export default Login;
