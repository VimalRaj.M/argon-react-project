import React from "react";
import Header from "components/Header.js";



const Maps = () => {
  return (
    <>
      <Header CanShowCardBody={false} />
      {/* Page content */}
      <div className=" container-fluid mt--7">
        <div className="row">
          <div className="col">
            <div className="card shadow border-0">
            <iframe title="myFrame" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.6600798865!2d77.35073573336324!3d12.954517008640543!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1652246885270!5m2!1sen!2sin" height="500" style={{border:"0"}} loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
              
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Maps;
