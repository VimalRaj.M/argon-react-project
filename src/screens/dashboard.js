import { useState } from "react";

import classnames from "classnames";

import Chart from "chart.js";

import { Line, Bar } from "react-chartjs-2";

import {

  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Col,
} from "reactstrap";

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2,
} from "libraries/charts.js";

import Header from "components/Header.js";

const PageVisitView = () => {
  return (<div className="mb-5 mb-xl-0 col-xl-8">
    <div className="card shadow">
      <div className="card-header border-0">
        <div className="row align-items-center">
          <div className="col">
            <h3 className="mb-0">Page visits</h3>
          </div>
          <div className="col text-right">
            <button
              className="btn-primary"
              color="primary"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
              size="sm"
            >
              See all
            </button>
          </div>
        </div>
      </div>
      <Table className="align-items-center table-flush" responsive>
        <thead className="thead-light">
          <tr>
            <th scope="col">Page</th>
            <th scope="col">Users</th>
            <th scope="col">Different users</th>
            <th scope="col">Bounce rate</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">/argon/index.html</th>
            <td>536</td>
            <td>40</td>
            <td>
              <i className="fas fa-arrow-up text-success mr-3" /> 63%
            </td>
          </tr>
          <tr>
            <th scope="row">/argon/</th>
            <td>385</td>
            <td>45</td>
            <td>
              <i className="fas fa-arrow-down text-warning mr-3" />{" "}
              46%
            </td>
          </tr>
          <tr>
            <th scope="row">/argon/profile.html</th>
            <td>513</td>
            <td>294</td>
            <td>
              <i className="fas fa-arrow-down text-warning mr-3" />{" "}
              39%
            </td>
          </tr>
          <tr>
            <th scope="row">/argon/tables.html</th>
            <td>250</td>
            <td>47</td>
            <td>
              <i className="fas fa-arrow-up text-success mr-3" /> 57%
            </td>
          </tr>
        </tbody>
      </Table>
    </div>
  </div>);
}


const SocialTrafficView = () => {
  return (<div className="col-xl-4">
    <div className="card shadow">
      <div className="card-header border-0">
        <div className="row align-items-center">
          <div className="col">
            <h3 className="mb-0">Social traffic</h3>
          </div>
          <div className="col text-right">
            <button
              className="btn-primary"
              color="primary"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
              size="sm"
            >
              See all
            </button>
          </div>
        </div>
      </div>
      <Table className="align-items-center table-flush" responsive>
        <thead className="thead-light">
          <tr>
            <th scope="col">Referral</th>
            <th scope="col">Visitors</th>
            <th scope="col" />
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">Instagram</th>
            <td>1980</td>
            <td>
              <div className="d-flex align-items-center">
                <span className="mr-2">50%</span>
                <div>
                  <Progress
                    max="100"
                    value="60"
                    barClassName="bg-gradient-danger"
                  />
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <th scope="row">Facebook</th>
            <td>340</td>
            <td>
              <div className="d-flex align-items-center">
                <span className="mr-2">60%</span>
                <div>
                  <Progress
                    max="100"
                    value="70"
                    barClassName="bg-gradient-success"
                  />
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <th scope="row">Google</th>
            <td>807</td>
            <td>
              <div className="d-flex align-items-center">
                <span className="mr-2">85%</span>
                <div>
                  <Progress max="100" value="80" />
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <th scope="row">Twitter</th>
            <td>2,645</td>
            <td>
              <div className="d-flex align-items-center">
                <span className="mr-2">30%</span>
                <div>
                  <Progress
                    max="100"
                    value="30"
                    barClassName="bg-gradient-warning"
                  />
                </div>
              </div>
            </td>
          </tr>
        </tbody>
      </Table>
    </div>
  </div>);
}


const PerformanceView = () => {
  return (<div className="col-xl-4">
    <div className="card shadow">
      <div className="card header bg-transparent">
        <div className="row align-items-center">
          <div className="col">
            <h6 className="text-uppercase text-muted ls-1 mb-1">
              Performance
            </h6>
            <h2 className="mb-0">Total orders</h2>
          </div>
        </div>
      </div>
      <div className="card-body">
        {/* Chart */}
        <div className="chart">
          <Bar
            data={chartExample2.data}
            options={chartExample2.options}
          />
        </div>
      </div>
    </div>
  </div>);
}

const Index = (props) => {
  const [activeNav, setActiveNav] = useState(1);
  const [chartExample1Data, setChartExample1Data] = useState("data1");

  if (window.Chart) {
    parseOptions(Chart, chartOptions());
  }

  const toggleNavs = (e, index) => {
    e.preventDefault();
    setActiveNav(index);
    setChartExample1Data("data" + index);
  };
  return (
    <>
      <Header />
      {/* Page content */}
      <div className="container-fluid mt--7">
        <div className="row">
          <Col className="mb-5 mb-xl-0" xl="8">
            <div className="card bg-gradient-default shadow">
              <div className="card-header bg-transparent">
                <div className="row align-items-center">
                  <div className="col">
                    <h6 className="text-uppercase text-light ls-1 mb-1">
                      Overview
                    </h6>
                    <h2 className="text-white mb-0">Sales value</h2>
                  </div>
                  <div className="col">
                    <Nav className="justify-content-end" pills>
                      <NavItem>
                        <NavLink
                          className={classnames("py-2 px-3", {
                            active: activeNav === 1,
                          })}
                          href="#pablo"
                          onClick={(e) => toggleNavs(e, 1)}
                        >
                          <span className="d-none d-md-block">Month</span>
                          <span className="d-md-none">M</span>
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames("py-2 px-3", {
                            active: activeNav === 2,
                          })}
                          data-toggle="tab"
                          href="#pablo"
                          onClick={(e) => toggleNavs(e, 2)}
                        >
                          <span className="d-none d-md-block">Week</span>
                          <span className="d-md-none">W</span>
                        </NavLink>
                      </NavItem>
                    </Nav>
                  </div>
                </div>
              </div>
              <div className="card-body">
                {/* Chart */}
                <div className="chart">
                  <Line
                    data={chartExample1[chartExample1Data]}
                    options={chartExample1.options}
                    getDatasetAtEvent={(e) => console.log(e)}
                  />
                </div>
              </div>
            </div>
          </Col>
          <PerformanceView />
        </div>
        <div className="row mt-5">
          <PageVisitView />
          <SocialTrafficView />
        </div>
      </div>
    </>
  );
};

export default Index;
