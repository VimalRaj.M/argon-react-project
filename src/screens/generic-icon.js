
import { CopyToClipboard } from "react-copy-to-clipboard";

import {
    UncontrolledTooltip,
} from "reactstrap";



const GenericIconBlock = ({ copiedText, onCopyText, id, iconStyle, label }) => {
    // const [copiedText1, setCopiedText1] = useState('');
    return (
        <div className="col-lg-3 col-md-6" lg="3" md="6">
            <CopyToClipboard
                text={iconStyle}
                onCopy={() => {
                    // setCopiedText1(iconStyle);

                    onCopyText(iconStyle);
                    console.log(copiedText);

                }}
            >
                <button
                    className="btn-icon-clipboard"
                    data-clipboard-text={label}
                    id={id}
                    type="button"
                >
                    <div>
                        <i className={iconStyle} />
                        <span>{label}</span>
                    </div>
                </button>
            </CopyToClipboard>
            <UncontrolledTooltip
                delay={0}
                trigger="hover focus"
                target={id}
            >
                {copiedText === { iconStyle }
                    ? "Copied!"
                    : "Copy "}
            </UncontrolledTooltip>
        </div>);
}


export default GenericIconBlock;